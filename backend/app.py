from __future__ import print_function

from flask import Flask,jsonify,session,request
from api.models import Node as NodeModel,Edge as EdgeModel ,to_dict,db
from api.config import Config
from pprint import pprint
from flask_cors import CORS
from api.cache import Cache
import time
import sys
#print('no cache', file=sys.stderr)



def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(Config)
    db.init_app(app)

    @app.route('/childs', methods=['POST'])
    def getNodesByParnet():
        res =  request.get_json(silent=True)
        error = validateInput(res)
        if error is not None:
            return jsonify(error)
        parentId=res['parentId']
        childs = cache.get(parentId)
        if childs is not None:
            return childs
        else:
            nodes = [to_dict(node) for node in NodeModel.query.join(EdgeModel).filter(EdgeModel.parent_id ==parentId).all()]
            json = jsonify({'nodes':nodes})
            cache.set(parentId,json)
        return json

    @app.route('/root')
    def getRootNode():
        root = cache.get('root')
        if root is not None:
            return root
        else:
            root = to_dict(NodeModel.query.outerjoin(EdgeModel).filter(EdgeModel.parent_id ==None).first())
            json = jsonify({'root':root})
            cache.set('root',json)
        return json

    def validateInput(res):
        if 'parentId' not in res:
            return {'error':'missing parentId'}
        else:
            parentId=res['parentId']

        #if not parentId.strip():
        #        return {'error':'parentId is empty'}
        if not isinstance(parentId,int):
            return {'error':'parentId is not a unsigned number'}
        return None

    return app


cache =  Cache()
app = create_app()


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
    data['data']['status'] = 'error'
    data['data']['error'] = msg
