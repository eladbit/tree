import os
import tempfile

import pytest

from app import create_app
import pytest


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app()
    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = flask_app.test_client()
    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()
    yield testing_client  # this is where the testing happens!
    ctx.pop()


def test_get_root_node(test_client):

    response = test_client.get('/root')
    assert response.status_code == 200
    json_data = response.get_json()
    assert 'winterfell.westeros.got' == json_data['root']['node_name']


def test_get_node_by_parent(test_client):
    response = test_client.post('/childs', json={
        'parentId': '1'
    })
    assert response.status_code == 200
    json_data = response.get_json()
    assert 'Computers' == json_data['nodes'][0]['node_name']

def test_get_node_by_parent_empty_parentId(test_client):

    response = test_client.post('/childs', json={
        'parentId':''
    })
    assert response.status_code == 200
    json_data = response.get_json()
    assert 'parentId is empty' == json_data['error']


def test_get_node_by_parent_no_parentId(test_client):
    response = test_client.post('/childs', json={

    })
    assert response.status_code == 200
    json_data = response.get_json()
    assert 'missing parentId' == json_data['error']



def test_get_node_by_parent_not_valid_parentId(test_client):

    response = test_client.post('/childs', json={
        'parentId':'jj'
    })
    assert response.status_code == 200
    json_data = response.get_json()
    assert 'parentId is not a unsigned number' == json_data['error']
