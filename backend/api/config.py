import os

class Config(object):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://myuser:mypassword@'+os.environ["DB_HOST"]+'/tree'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
