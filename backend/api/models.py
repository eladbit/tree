from __future__ import print_function

import json
import sys


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import DeclarativeMeta

db = SQLAlchemy()

class Node(db.Model):
    __tablename__ = 'nodes'
    node_id = db.Column(db.Integer, nullable=False, primary_key=True)
    node_name = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return '<Node %r>' % self.node_id + ' ' + self.node_name

class Edge(db.Model):
    __tablename__ = 'edges'
    parent_id = db.Column(db.Integer, nullable=False, primary_key=True)
    child_id = db.Column(db.Integer,db.ForeignKey("nodes.node_id"), nullable=False,primary_key=True)
    child = db.relation(Node, backref='edges',foreign_keys=child_id )

    def __repr__(self):
        return '<Node %r>' % self.parent_id + ' ' + self.child_id


def to_dict(obj):

    if isinstance(obj.__class__, DeclarativeMeta):
        # an SQLAlchemy class
        fields = {}
        for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:

            data = obj.__getattribute__(field)

            try:
                json.dumps(data)  # this will fail on non-encodable values, like other classes
                if data is not None:
                    fields[field] = data
            except TypeError:
                pass
        # a json-encodable dict

        return fields
