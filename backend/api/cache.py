
class Cache():
    def __init__(self):
        self.__cache ={}

    def get(self,node_id):
        return self.__cache.get(node_id,None) 

    def set(self,node_id,childs):
        self.__cache[node_id]=childs
