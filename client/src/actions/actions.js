import Server from '../api/Server'

/*
 * action types
 */

export const GET_ROOT_NODE = 'GET_ROOT_NODE';
export const GET_NODE_CHILDREN = 'GET_NODE_CHILDREN';
export const NODE_LOADING = 'NODE_LOADING';
export const TOGGLE_FOLDER = 'TOGGLE_FOLDER';



/*
 * action creators
 */

export function getNodeChildren(nodeId) {
  return async (dispatch) => {
    dispatch(nodeLoading(nodeId))
    let childs = await Server.getNodeChilds(nodeId)
    return dispatch({ type: GET_NODE_CHILDREN,nodeId,childs})
  }
}


export function getRootNode() {
  return async (dispatch) => {
    dispatch(nodeLoading())
    let node =  await Server.getRootNode();
    return dispatch({ type: GET_ROOT_NODE,node })
  }
}


export function toggleFolder  (nodeId) {
  return async (dispatch) => {
    return dispatch({ type: TOGGLE_FOLDER,nodeId })
  }
}


const nodeLoading=(nodeId) => ({
  type: NODE_LOADING, nodeId
})
