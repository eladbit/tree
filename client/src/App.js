import React, { Component } from 'react';
import { createStore ,applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk'

import nodesReducer from './reducers/reducers'
import './App.css';
import Tree from './components/Tree';

const store = createStore(nodesReducer,applyMiddleware(thunkMiddleware));

class App extends Component {

  render() {

    return (
      <Provider store={store}>
        <div style={{padding:40}}>
            <h1>Tree Nodes list</h1>
              <Tree  />
        </div>
      </Provider>
    );
  }
}

export default App;
