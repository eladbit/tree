import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16'
import {Tree} from '../../components/Tree'
import configureMockStore from 'redux-mock-store'


Enzyme.configure({ adapter: new Adapter() })


const mockStore = configureMockStore()
const store = mockStore({nodes:{
  1:{node_id:1,node_name:"name1",root:true}
}})

const props = {
  fetchRootNode: jest.fn(),
  nodes:{1:{node_id:1,node_name:"name1",root:true}}
}

describe('<Tree />', () => {
    it('renders the component', () => {
      const wrapper = shallow(<Tree {...props} store={store}/>);
      const component = wrapper
      expect(toJson(component)).toMatchSnapshot();
    });

});
