import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16'
import {TreeNode} from '../../components/TreeNode'


Enzyme.configure({ adapter: new Adapter() })



const getChildNodes = jest.fn();

getChildNodes.mockReturnValueOnce({
  onToggle: jest.fn(),
  getChildNodes :jest.fn(),
  node: [{node_id:2,node_name:"name2"}]
})



const props = {
  onToggle: jest.fn(),
  getChildNodes :getChildNodes,
  node: {node_id:1,node_name:"name1",root:true,ioOpen}
}

describe('<TreeNode />', () => {
    it('renders the component', () => {

      const wrapper = shallow(<TreeNode {...props} />);
      const component = wrapper

      expect(toJson(component)).toMatchSnapshot();
    });



    it('successfully calls the onToggle handler', () => {
      const mockOnClick = jest.fn();
      const wrapper = shallow(
        <TreeNode {...props} />
      );
      const component = wrapper

      component.find('#toggleIcon').simulate('click');

      expect(props.onToggle.mock.calls.length).toEqual(1);
    });
  });
