import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../actions/actions'
import fetchMock from 'fetch-mock'
import { CONFIG } from '../../config.js';
//import expect from 'expect' // You can use any testing library

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('test actions', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates GET_ROOT_NODE when fetching rootNode has been done', () => {
    fetchMock.getOnce(CONFIG.API_BASE_URL+"/root", {root:
      {edges:[],node_id:1,node_name: "winterfell.westeros.got"}
    })

    const expectedActions = [
      { type: actions.NODE_LOADING,nodeId: undefined },
      { type: actions.GET_ROOT_NODE, node: {edges:[],node_id:1,node_name: "winterfell.westeros.got"}}
    ]
    const store = mockStore({
      nodes:{

      },
      loading:false
    })

    return store.dispatch(actions.getRootNode()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates GET_NODE_CHILDREN when fetching nodeChilds has been done', () => {
    fetchMock.postOnce(CONFIG.API_BASE_URL+"/childs", {nodes:
      [{edges:[],node_id:1,node_name: "winterfell.westeros.got"},{edges:[],node_id:1,node_name: "winterfell.westeros.got"}]
    })

    const expectedActions = [
      { type: actions.NODE_LOADING,nodeId: 1 },
      { type: actions.GET_NODE_CHILDREN ,nodeId: 1, childs:[{edges:[],node_id:1,node_name: "winterfell.westeros.got"},{edges:[],node_id:1,node_name: "winterfell.westeros.got"}]}
    ]
    const store = mockStore({
      nodes:{

      },
      loading:false
    })

    return store.dispatch(actions.getNodeChildren(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })


  it('creates TOOGLE_FOLDER', () => {
    const expectedActions = [
      { type: actions.TOGGLE_FOLDER,nodeId: 1 },
    ]
    const store = mockStore({
      nodes:{
          1:  {edges:[],node_id:1,node_name: "winterfell.westeros.got"}
      },
      loading:false
    })

    return store.dispatch(actions.toggleFolder(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

})
