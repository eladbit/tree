import Server from '../../api/Server'
import fetchMock from 'fetch-mock'
import { CONFIG } from '../../config.js';


describe('Test server api', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('Test getNodeChilds', () => {
    fetchMock.postOnce(CONFIG.API_BASE_URL+"/childs",
      {nodes:[{1:{node_name:"name",node_id:1}}]}

    )

    return Server.getNodeChilds().then(resp =>
      expect(resp).toEqual([{1:{node_name:"name",node_id:1}}])
    )
  })


  it('Test getNodeChilds failed', () => {
    fetchMock.postOnce(CONFIG.API_BASE_URL+"/childs", {error:
      "missing parent Id"
    })

    return Server.getNodeChilds().then(resp =>
      expect(resp).toEqual([])
    )
  })

  it('Test getRootNode', () => {
    fetchMock.getOnce(CONFIG.API_BASE_URL+"/root",
      {root:{1:{node_name:"name",node_id:1}}}

    )

    return Server.getRootNode().then(resp =>
      expect(resp).toEqual({1:{node_name:"name",node_id:1}})
    )
  })


  it('Test getRootNode no results', () => {
    fetchMock.getOnce(CONFIG.API_BASE_URL+"/root", {

    })

    return Server.getRootNode().then(resp =>
      expect(resp).toEqual(undefined)
    )
  })

})
