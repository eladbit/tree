import reducer from '../../reducers/reducers'
import * as actions from '../../actions/actions'

describe('reducers', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        nodes:{},loading:false
      }
    )
  })

  it('should handle NODE_LOADING for root', () => {
    expect(
      reducer({nodes:{1:{},2:{}}} , {
        type: actions.NODE_LOADING,
        nodeId: 1
      })
    ).toEqual(
      {nodes:{1:{loading:true},2:{}}}
    )
  })

  it('should handle NODE_LOADING for children', () => {
    expect(
      reducer({nodes:{}} , {
        type: actions.NODE_LOADING,
        nodeId: undefined
      })
    ).toEqual(
      {loading:true}
    )
  })


  it('should handle GET_NODE_CHILDREN ', () => {
    expect(
      reducer({nodes:{1:{},2:{loading:true}}} , {
        type: actions.GET_NODE_CHILDREN,
        nodeId: 2,
        childs:[{node_id:3},{node_id:4}]
      })
    ).toEqual(
      {nodes:{
          1:{},
          2:{loading:false,children:[3,4],isOpen:true},
          3:{node_id:3},
          4:{node_id:4}
        }}
    )
  })

  it('should handle GET_ROOT ', () => {
    expect(
      reducer({nodes:{},loading:true} , {
        type: actions.GET_ROOT_NODE,
        nodeId: 1,
        node:{node_id:1}
      })
    ).toEqual(
      {loading:false,
        nodes:{
          1:{root:true,node_id:1},
      }}
    )
  })

  it('should handle TOGGLE_FOLDER ', () => {
    expect(
      reducer({nodes:{
        1:{root:true,node_id:1,isOpen:false}
      }} , {
        type: actions.TOGGLE_FOLDER,
        nodeId: 1,
      })
    ).toEqual(
      {
        nodes:{
          1:{root:true,node_id:1,isOpen:true},
      }}
    )
  })

})
