import React, { Component } from 'react';
import values from 'lodash/values';
import { connect } from 'react-redux'
import { getNodeChildren,getRootNode,toggleFolder } from '../actions/actions'

import TreeNode from './TreeNode';
import ReactLoading from 'react-loading';


export class Tree extends Component {



  componentDidMount() {

   this.props.fetchRootNode();

  }


  getRootNode = () => {
    const { nodes } = this.props;
    let root =  values(nodes).filter(node => node.root);
    return root.length?root[0]:null
  }

   getChildNodes = (node) => {
    const { nodes } = this.props;
    if (!node.children){
         return [];
    }
    return node.children.map(id => nodes[id]);
  }


  onToggle = async (node) => {
    if (!node.children){
      this.props.fetchNodeChildren(node.node_id)
     }else {
       this.props.toggleFolder(node.node_id)
     }
  }


  render() {
    const rootNode = this.getRootNode();
    return (
      <div>
      {this.props.loading && <ReactLoading type={'spin'} color={'black'} height={15} width={15}  /> }
      {rootNode &&
          <TreeNode
            id="treeNode"
            node={rootNode}
            getChildNodes={this.getChildNodes}
            onToggle={this.onToggle}
            onNodeSelect={this.onNodeSelect}
          />
        }
      </div>
    )
  }
}


const mapStateToProps = state => ({
  nodes: state.nodes,
  loading:state.loading
})


const mapDispatchToProps = dispatch => ({
   fetchNodeChildren:(nodeId)=>dispatch(getNodeChildren(nodeId)),
   fetchRootNode:()=>dispatch(getRootNode()),
   toggleFolder:(nodeId)=>dispatch(toggleFolder(nodeId))

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tree)
