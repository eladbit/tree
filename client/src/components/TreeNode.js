import React from 'react';
import {FaGlobe, FaFolder, FaFolderOpen, FaChevronDown, FaChevronRight } from 'react-icons/fa';
import ReactLoading from 'react-loading';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const getPaddingLeft = (level) => {
  let paddingLeft = level * 20;
  return paddingLeft;
}

const StyledTreeNode = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 5px 8px;
  padding-left: ${props => getPaddingLeft(props.level)}px;
`;

const NodeIcon = styled.div`
  font-size: 12px;
  margin-right: ${props => props.marginRight ? props.marginRight : 5}px;
`;


export const TreeNode = (props) => {
  const { node, getChildNodes, level, onToggle } = props;
  return (
    <React.Fragment>
      <StyledTreeNode level={level}>
        <NodeIcon id={'toggleIcon'} onClick={() => onToggle(node)}>
          { (node.isOpen ? <FaChevronDown /> : <FaChevronRight />) }
        </NodeIcon>

        <NodeIcon marginRight={10}>
          { node.loading && <ReactLoading type={'spin'} color={'black'} height={15} width={15}  /> }
          { !node.loading && node.root && <FaGlobe /> }
          { !node.loading && !node.root  && node.isOpen  && <FaFolderOpen /> }
          { !node.loading && !node.root  && !node.isOpen && <FaFolder /> }
        </NodeIcon>


        <span>
          {  node.node_name }
        </span>
      </StyledTreeNode>

      { node.isOpen && getChildNodes(node).map((childNode,i) => (
        <TreeNode
          key={i}
          {...props}
          node={childNode}
          level={level + 1}
        />
      ))}
    </React.Fragment>
  );
}

TreeNode.propTypes = {
  node: PropTypes.object.isRequired,
  getChildNodes: PropTypes.func.isRequired,
  level: PropTypes.number.isRequired,
  onToggle: PropTypes.func.isRequired,
};

TreeNode.defaultProps = {
  level: 0,
};

export default TreeNode;
