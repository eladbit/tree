import {
  GET_NODE_CHILDREN,
  GET_ROOT_NODE,
  NODE_LOADING,
  TOGGLE_FOLDER

} from '../actions/actions'



const initialState = {
  nodes:{

  },
  loading:false

}



const nodesReducer = (state = initialState, action) => {
  let {nodeId,node,childs} = action
  let newNodes = {...state.nodes}
  let newNode=null;
  switch(action.type) {

    case GET_NODE_CHILDREN:
        newNode = newNodes[nodeId]
        newNode.children=childs.map(c=>c.node_id);
        newNode.isOpen = true;
        newNode.loading=false;
        newNodes[nodeId]=newNode;
        for(let c of childs)
        {
          newNodes[c.node_id]=c
        }
        return {nodes:newNodes}

    case GET_ROOT_NODE:
      if(node)
      {
        node.root=true;
        newNodes[node.node_id]=node;
      } 
      return {nodes:newNodes,loading:false}

    case NODE_LOADING:
      if(nodeId)
      {
        newNodes[nodeId].loading=true;
        return {nodes:newNodes}
      }
      else {
        return {loading:true}
      }


    case TOGGLE_FOLDER:
        newNode = newNodes[nodeId]
        newNode.isOpen = !newNode.isOpen;
        newNodes[nodeId]=newNode;
        return {nodes:newNodes}

      default:
        return state
    }

  }


export default nodesReducer
