import { CONFIG } from '../config.js';

class Server{


  async getNodeChilds(id)
  {
    let results=  await fetch(CONFIG.API_BASE_URL+"/childs",{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({parentId:id})
    });
    try
    {
      results = await results.json();
      if(results.error)
      {
        console.error(results.error)
        return[];
      }
      return results?results.nodes:[];
    }
    catch(e){
      return[];
    }
  }


  async getRootNode(id)
  {
    try{
      let results=  await fetch(CONFIG.API_BASE_URL+"/root");
      results = await results.json();
      return results?results.root:null;
    }
    catch(e){
      return null;
    }
  }

}


const server = new Server();
export default server;
