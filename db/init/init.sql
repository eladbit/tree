CREATE TABLE "nodes" (
	"node_id" INTEGER NOT NULL,
	"node_name" VARCHAR(100) NOT NULL,
	PRIMARY KEY ("node_id")
);

INSERT INTO nodes VALUES (1, 'winterfell.westeros.got');
INSERT INTO nodes VALUES (2, 'Computers');
INSERT INTO nodes VALUES (3, 'Domain Controllers');
INSERT INTO nodes VALUES (4, 'TheWall');
INSERT INTO nodes VALUES (5, 'Kylo-Ou');



CREATE TABLE "edges" (
	"parent_id" INTEGER NOT NULL,
	"child_id" INTEGER NOT NULL,
	PRIMARY KEY ("parent_id", "child_id")
);

INSERT INTO edges VALUES (1, 2);
INSERT INTO edges VALUES (1, 3);
INSERT INTO edges VALUES (1, 4);
INSERT INTO edges VALUES (4, 5);
